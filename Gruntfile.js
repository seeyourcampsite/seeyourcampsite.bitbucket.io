module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    browserSync: {
        bsFiles: {
            src : [
                'css/*.css',
                '*.html'
            ]
        },
        options: {
            server: {
                baseDir: "./"
            }
        }
    }
  });

  // Plugin that provides the "browsersync" task.
  grunt.loadNpmTasks('grunt-browser-sync');

  // Default task(s).
  grunt.registerTask('default', ['browserSync']);

}