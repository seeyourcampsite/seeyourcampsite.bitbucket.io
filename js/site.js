/** search bar toggle */
function makeStateSearch() {
  document.getElementById('keywordSearch').style.display='none';
  document.getElementById('stateSearch').style.display='block';
}

function makeKeywordSearch() {
  document.getElementById('stateSearch').style.display='none';
  document.getElementById('keywordSearch').style.display='block';
}

/** admin campground search toggle */
function showAdminCgSearchField() {
  document.getElementById('adminCgName').style.display='none';
  document.getElementById('adminCgSearchBtn').style.display='none';
  document.getElementById('adminCgSearchField').style.display='block';
  document.getElementById('adminCgHideSearchBtn').style.display='block';
}

function hideAdminCgSearchField() {
  document.getElementById('adminCgName').style.display='block';
  document.getElementById('adminCgSearchBtn').style.display='block';
  document.getElementById('adminCgSearchField').style.display='none';
  document.getElementById('adminCgHideSearchBtn').style.display='none';
}

/** admin review table toggles */
function viewReview() {
  document.getElementById('review3').style.display='none';
  document.getElementById('review3A').style.display='table-row';
  document.getElementById('review3B').style.display='table-row';
  document.getElementById('review3C').style.display='table-row';
}

function closeReview() {
  document.getElementById('review3A').style.display='none';
  document.getElementById('review3B').style.display='none';
  document.getElementById('review3C').style.display='none';
  document.getElementById('review3').style.display='table-row';
}

/** admin manage campsites toggle */
function manageMultipleSites() {
  document.getElementById('manageMultipleSites').style.display='block';
  document.getElementById('manageIndividualSites').style.display='none';
}

function manageIndividualSites() {
  document.getElementById('manageMultipleSites').style.display='none';
  document.getElementById('manageIndividualSites').style.display='block';
}

/** sticky header for campground page using Waypoints */

$(function () {
  
  var cgHeaderAffixedWrap = $('#cgHeaderAffixedWrap');
  var cgHeaderAffixed = $('#cgHeaderAffixed');

  cgHeaderAffixedWrap.waypoint({
    handler: function (event, direction) {
        cgHeaderAffixed.toggleClass('stuck', direction='down');
        if (direction = 'down')
          cgHeaderAffixedWrap.css({ 'height':cgHeaderAffixed.outerHeight() });
        else
          cgHeaderAffixedWrap.css({ 'height':'auto' });
      }
  });
});

/** page navigation Waypoints */
// over my head...
$(function() {
  var overview = $('#overview');
  var photos = $('#photos');
  var features = $('#features');
  var reviews = $('#reviews');
  var map = $('#map');

  /** ?????? */
});

/** snippet to trigger sticky header on page resize via class change 
// but I can't get it to work...

$(document).ready(function() {
    // run test on initial page load
    checkSize();

    // run test on resize of the window
    $(window).resize(checkSize);
});

//Function to the css rule
function checkSize(){
    if ($(".sampleClass").css("float") == "none" ){
        // your code here
    }
}
*/



